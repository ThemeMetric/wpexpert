<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Wpexpert
 */

?>
 <footer id="footer" class="section" role="contentinfo">
            <div>
                <div class="upper clearfix">    
                    
                <?php dynamic_sidebar( 'footer-4' ); ?>    


                </div>
                <div class="lower">
                    <div class="logo">
                    <?php $logo_footer=get_theme_mod('footer_logo');?>
                    <?php if ($logo_footer) { ?>
                        <a href="<?php bloginfo('url');  ?>"><img src="<?php echo $logo_footer; ?>" alt="<?php bloginfo( 'name' ); ?>" width="230" height="40" data-fallback="images/logo.svg" /></a>

                    <?php } else { ?>
                        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
                        <h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
                    <?php } ?>
                    </div>
                     <?php if ( has_nav_menu( 'menu-footer' ) ) {
                        wp_nav_menu( array(
                                'menu'              => 'menu-footer',
                                'theme_location'    => 'menu-footer',                         
                                'menu_class'        => 'links',
                                
                            )
                        );
                    }
                    ?>
                    <ul class="social">
                       <?php if (get_theme_mod('googleplus')) : ?>                   
                        <li><a href="<?php echo get_theme_mod('googleplus');?>" target="_blank" title="Follow us on Google+" rel="noopener noreferrer"><span class="screen-reader-text">Follow us on Google+</span> <span class="fa fa-google-plus"></span></a></li>
                        <?php	else : ?>
                        <?php endif ;?>

                        <?php if (get_theme_mod('facebook')) : ?>
                        <li><a href="<?php echo get_theme_mod('facebook');?>" target="_blank" rel="noopener noreferrer" title="Follow us on Facebook"><span class="screen-reader-text">Follow us on Facebook</span> <span class="fa fa-facebook"></span></a></li>
                        <?php	else : ?>
                        <?php endif ;?>

                        <?php if (get_theme_mod('twitter')) : ?>
                        <li><a href="<?php echo get_theme_mod('twitter');?>" target="_blank" rel="noopener noreferrer" title="Follow us on Twitter"><span class="screen-reader-text">Follow us on Twitter</span> <span class="fa fa-twitter"></span></a></li>
                        <?php	else : ?>
                        <?php endif ;?>
                        
                        <?php if (get_theme_mod('linkdin')) : ?>
                        <li><a href="<?php echo get_theme_mod('linkdin');?>" target="_blank" rel="noopener noreferrer" title="Follow us on LinkedIn"><span class="screen-reader-text">Follow us on LinkedIn</span> <span class="fa fa-linkedin"></span></a></li>
                        <?php	else : ?>
                        <?php endif ;?>
                    </ul>
                </div>
            </div>
        </footer>
        </div>

        <div id="rock-bottom"></div>
				
<?php wp_footer(); ?>

</body>
</html>
