<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Wpexpert
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('summary-post'); ?>>

	<div class="entry-content clearfix">

<h1 class="heading"><?php the_title(); ?></h1>
		
<header class="clearfix">
      <div>
        <div class="author-photo">

<?php echo get_avatar( get_the_author_meta( 'ID' ), 96 ); ?>

							</div>
        <div class="deets">

         	<?php  wpexpert_posted_by();?>  	<?php the_date('F j, Y'); ?>	
          
          </div>
        <div class="social">
          <div>
            Share:


                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox"></div>
            
                
            

          </div>
                            
        </div>
      </div>
</header>

<?php the_content(); ?>

<footer>
<div class="categories">
Posted in: <?php $categories = get_the_category();
        if ( ! empty( $categories ) ) {
            echo '<a href="'.esc_url( get_category_link( $categories[0]->term_id ) ).'"><i class="fa fa-wpexplorer"></i> '.esc_html( $categories[0]->name ).'</a>';
        } ?>

</div>

<div class="author clearfix">
<div class="photo">
<?php echo get_avatar( get_the_author_meta( 'ID' ), 96 ); ?>	</div>
<div class="bio">
  <div>
    <h4>About <?php the_author() ; ?></h4>
    <div class="content">

    <?php the_author_meta( 'description' ); ?>
  </div>
</div>
</div>
</div>

 <div class="related-posts clearfix">
  <h3>Related Posts</h3>
  <div class="posts">
  <?php $orig_post = $post;
        global $post;
        $categories = get_the_category($post->ID);
        if ($categories) {
        $category_ids = array();
        foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
        $args=array(
        'category__in' => $category_ids,
        'post__not_in' => array($post->ID),
        'posts_per_page'=> 4,
        'orderby'             => 'rand',
        'ignore_sticky_posts'=>1
        );
        $my_query = new wp_query( $args );
        if( $my_query->have_posts() ) {
        while( $my_query->have_posts() ) {
        $my_query->the_post(); ?>
        						<div class="post">
											<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
											<div class="deets"><?php echo the_date('F j, Y'); ?></div>
											<p><?php the_excerpt(); ?></p>
										    </div>

        <?php
        }
      
        }
        }
        $post = $orig_post;
        wp_reset_query(); ?>


  </div>
</div>
        </footer>

	</div>
	<!-- .entry-content -->
	<footer class="entry-footer">
	</footer>
	<!-- .entry-footer -->

</article>
