<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package Wpexpert
 */

get_header();
?>

<?php if (get_theme_mod('blog_banner')) : ?>
<section id="banner" class="section generic-banner" style="background-image: url(<?php echo get_theme_mod('blog_banner');?>);">
<?php	else : ?>
<?php endif ;?>
<div>
<?php if (get_theme_mod('blog_heading')) : ?>
        <h1 class="heading"><?php echo get_theme_mod('blog_heading');?></h1>
        <?php	else : ?>
        <?php endif ;?>
        <?php if (get_theme_mod('blog_des')) : ?>
        <p><?php echo get_theme_mod('blog_des');?></p>
        <?php	else : ?>
        <?php endif ;?>
</div>
</section>
<section class="section white gutter">
<div>

<div class="sidebar-layout clearfix">
<div class="content">
<div>

<?php
while ( have_posts() ) : the_post();

        get_template_part( 'template-parts/content-single', get_post_format() );

        if ( comments_open() || get_comments_number() ) :
                comments_template();
        endif;
endwhile; 
?>
</div>
</div>
<aside>
<?php dynamic_sidebar( 'sidebar-1' ); ?>
</aside>
</div>
</div>
</section>
<?php get_footer();
